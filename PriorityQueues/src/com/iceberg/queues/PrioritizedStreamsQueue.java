package com.iceberg.queues;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

/**
 * This is a prioritized streams queue. The basic idea is that you can add elements at different
 * priorities, and the priorities affect when things come off the queue, but priority isn't absolute.
 * A whole set of items at a high priority cannot starve items at a lower priority, you just get more
 * of the higher priority items.
 * 
 * Put another way (the one that leads to the class name), you can have streams of items at
 * varying priorities and this pulls them at an appropriate balance from each priority stream.
 * 
 * The ratio is effectively going to be the ratio of the priorities to one another, so you'll get
 * 10 items at priority 10 at the same time you'd get 9 items of priority 9. They'll mostly be
 * interleaved. You'll get twice as many items at priority 8 as you will at priority 4.
 * 
 * It's basically implemented using addition to the max priority and a modulus on that. The remainder
 * is the priority of the next item, and if items at a given priority don't wrap, they don't get
 * pulled that time. If two items yield with the same remainder, the one with higher overall priority
 * still gets to go first. If this doesn't make sense immediately, check the unit tests for examples.
 * 
 * TODO: Note that this class is not thread safe. It should be made thread-safe, I just didn't feel
 * like adding that code right now.
 *
 * @param <T>
 */
public class PrioritizedStreamsQueue<T>
{
	// we track the total items added and removed, so we know our current possible queue size
	// because we have to cycle as many as highestPriority - lowestPriority times to just see
	// if there's something in any queue, we don't bother if we know it's empty
	private int totalItems = 0;
	
	// every time we create a PriorityStream, we add it here. we don't care about order in this one,
	// just want the list of all streams
	ArrayList<PriorityStream<T>> allStreams = new ArrayList<PriorityStream<T>>();
	
	// this is an indexed array of streams based on priority - offset
	// values are empty/null for which no items have been added at a given priority
	PriorityStream<T>[] streams;
	int offset;

	// equivalent to highest priority
	private int rolloverPriority;
	
	// this is items that have come off in a cycle and are ready to go
	TreeSet<PrioritizedStreamItem<T>> readyItems = new TreeSet<PrioritizedStreamItem<T>>(new PriorityStreamComparator<T>());

	@SuppressWarnings("unchecked")
	PrioritizedStreamsQueue(int lowestPriority, int highestPriority)
	{
		this.offset = lowestPriority;
		this.rolloverPriority = highestPriority;
		this.streams = new PriorityStream[highestPriority - lowestPriority + 1];
	}

	/**
	 * Add an item at the given priority.
	 * 
	 * If a stream didn't already exist at the requested priority, this is the time when we create it.
	 * 
	 * @param item
	 * @param priority
	 */
	public void add(T item, int priority)
	{
		validatePriority(priority);
		
		if (streams[priority - offset] == null)
		{
			allocateStream(priority);
		}
		
		streams[priority - offset].add(item);
		totalItems++;
	}

	/**
	 * Reads the next prioritized item.
	 * If no items are available in the queue, returns immediately with a null.
	 * 
	 * @return
	 */
	public T read()
	{
		// if there's something still sitting in the queue, return it
		if (readyItems.size() > 0) return readyItems.pollLast().item;
		
		// if we potentially have items to return, cycle until we get the next highest priority items in the queue
		if (totalItems > 0)
		{
			while (readyItems.size() == 0) this.cycle();
			return readyItems.pollLast().item;
		}
		
		// or we have nothing
		return null;
	}

	/**
	 * Returns but does not consume the next highest priority item.
	 * If there's nothing in the queue, return null.
	 * @return
	 */
	public T peek()
	{
		// if there's something still sitting in the queue, return it
		if (readyItems.size() > 0) return readyItems.last().item;
		
		// if we potentially have items to return, cycle until we get the next highest priority items in the queue
		if (totalItems > 0)
		{
			while (readyItems.size() == 0) this.cycle();
			return readyItems.last().item;
		}
		
		// or we have nothing
		return null;
	}

	/**
	 * Clears all items from the queue.
	 */
	public void clear()
	{
		readyItems.clear();
		
		for (PriorityStream<T> stream : allStreams)
		{
			stream.clear();
		}
		
		totalItems = 0;
	}

	/**
	 * Clears all items from the queue at the given priority.
	 * 
	 * @param priority
	 * @return
	 */
	public int clear(int priority)
	{
		validatePriority(priority);

		int count = 0;
		
		// clear stream backlog at this priority
		if (streams[priority - offset] != null)
		{
			count = streams[priority - offset].clear();
			totalItems -= count;
		}
		
		// clear readyItems at this priority (there can be only one)
		Iterator<PrioritizedStreamItem<T>> iter = readyItems.iterator();
		while (iter.hasNext())
		{
			PrioritizedStreamItem<T> item = iter.next();
			if (item.streamPriority == priority)
			{
				iter.remove();
				count++;
				break;
			}
		}
		
		return count;
	}

	/**
	 * Returns current count of items in the queue.
	 * @return
	 */
	public int count()
	{
		return totalItems + readyItems.size();
	}

	/**
	 * Intended to wait until an item is available, but requires implementing thread
	 * safety and blocking and all that stuff ... since my goal was just to make
	 * sure the queue worked, I didn't do that (yet).
	 * @param timeout
	 * @param unit
	 * @return
	 */
	public T poll(long timeout, TimeUnit unit)
	{
		// I just didn't feel like writing all the code to do a working blocking timeout
		// that also might mean having to add some level of thread-safety to this class,
		// also something I didn't feel like doing for a prototype
		// TODO: implement polling with timeout
		throw new java.lang.UnsupportedOperationException();
	}

	/**
	 * Useful after a clear, which does not cause a reset, this resets all streams
	 * to a default state (no remainders present).
	 */
	public void reset()
	{
		for (PriorityStream<T> stream : allStreams)
		{
			stream.reset();
		}
	}
	
	/**
	 * Creates a new stream at the given priority and adds to the appropriate structures.
	 * We do this dynamically as users add to a given stream priority.
	 * 
	 * @param priority
	 */
	private void allocateStream(int priority)
	{
		PriorityStream<T> stream = new PriorityStream<T>(priority, rolloverPriority);
		
		allStreams.add(stream);
		streams[priority - offset] = stream;
	}

	/**
	 * Cycles each priority stream.
	 * Streams should add to the readyItem queue if cycle results in that.
	 * Update totalItems every time we get something.
	 */
	private void cycle()
	{
		for (PriorityStream<T> stream : allStreams)
		{
			PrioritizedStreamItem<T> outputItem = stream.cycle();
			if (outputItem != null)
			{
				totalItems--;
				readyItems.add(outputItem);
			}
		}
	}

	private void validatePriority(int priority)
	{
		if ((priority < offset) || (priority > rolloverPriority))
		{
			throw new IllegalArgumentException("Given priority " + priority + " is outside the valid range of: [" + offset + ", " + rolloverPriority + "]");
		}
	}

}

package com.iceberg.queues;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PrioritizedStreamsQueueTest
{

	PrioritizedStreamsQueue<String> queue;
	
	@Before
	public void setUp() throws Exception
	{
		queue = null;
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void test_two_items_very_different_priority()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);
		queue.add("def", 1);
		
		assertEquals("abc", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_two_items_same_priority_maintains_order()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 3);
		queue.add("def", 3);
		
		assertEquals("abc", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_two_items_different_priority_same_cycle_maintains_order()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 4);
		queue.add("def", 3);
		
		assertEquals("abc", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_two_items_different_priority_same_cycle_maintains_order2()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("def", 3);
		queue.add("abc", 4);
		
		assertEquals("abc", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_clear()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 4);
		queue.add("def", 3);
		queue.clear();
		
		assertEquals(null, queue.read());
	}

	@Test
	public void test_second_item_in_cycle_with_lower_order()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 4);
		
		// these two both yield second cycle, but def should be higher cycle priority even though lower stream priority
		queue.add("ghi", 4);
		queue.add("def", 3);
		
		assertEquals("abc", queue.read());
		assertEquals("def", queue.read());
		assertEquals("ghi", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_several_items_max_priority()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);
		queue.add("ghi", 5);
		queue.add("def", 5);
		
		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_several_items_min_priority()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 1);
		queue.add("ghi", 1);
		queue.add("def", 1);
		
		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_two_items_at_each_of_several_priorities()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);	// 0
		queue.add("def", 5);	// - 0
		queue.add("ghi", 4);	// - 3
		queue.add("jkl", 4);	// - - 2
		queue.add("mno", 3);	// - 1
		queue.add("pqr", 3);	// - - - 2
		queue.add("stu", 2);	// - - 1
		queue.add("vwx", 2);	// - - - - 0		// these two items yield same priority at same time
		queue.add("111", 1);	// - - - - 0		// these two items yield same priority at same time
		queue.add("222", 1);	// - - - - - x

		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		assertEquals("mno", queue.read());
		assertEquals("def", queue.read());
		assertEquals("jkl", queue.read());
		assertEquals("stu", queue.read());
		assertEquals("pqr", queue.read());
		assertEquals("vwx", queue.read());
		assertEquals("111", queue.read());
		assertEquals("222", queue.read());
		assertEquals(null, queue.read());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_add_priority_too_low()
	{
		queue = new PrioritizedStreamsQueue<String>(3, 5);
		queue.add("abc", 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void test_add_priority_too_high()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 6);
	}
	
	@Test
	public void test_peek_one_item()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);
		
		assertEquals("abc", queue.peek());
		assertEquals("abc", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_peek_mid_stream()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);
		queue.add("ghi", 5);
		queue.add("def", 5);
		
		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.peek());
		assertEquals("ghi", queue.read());
		assertEquals("def", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_clear_all()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);	// 0
		queue.add("def", 5);	// - 0
		queue.add("ghi", 4);	// - 3

		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		queue.clear();
		assertEquals(null, queue.read());
	}

	@Test
	public void test_clear_priority_not_in_ready_queue()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);	// 0
		queue.add("def", 5);	// - 0
		queue.add("ghi", 4);	// - 3
		queue.add("jkl", 4);	// - - 2
		queue.add("mno", 3);	// - 1
		queue.add("pqr", 3);	// - - - 2
		queue.add("stu", 2);	// - - 1

		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		queue.clear(3);
		assertEquals("def", queue.read());
		assertEquals("jkl", queue.read());
		assertEquals("stu", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_clear_priority_with_items_in_ready_queue()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);	// 0
		queue.add("def", 5);	// - 0
		queue.add("ghi", 4);	// - 3
		queue.add("jkl", 4);	// - - 2
		queue.add("mno", 3);	// - 1
		queue.add("pqr", 3);	// - - - 2
		queue.add("stu", 2);	// - - 1
		queue.add("vwx", 2);	// - - - - 0		// these two items yield same priority at same time

		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		queue.clear(3);
		assertEquals("def", queue.read());
		assertEquals("jkl", queue.read());
		assertEquals("stu", queue.read());
		assertEquals("vwx", queue.read());
		assertEquals(null, queue.read());
	}

	@Test
	public void test_count_empty()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		assertEquals(0, queue.count());
	}

	@Test
	public void test_count_after_clear()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);	// 0
		queue.add("def", 5);	// - 0
		queue.add("ghi", 4);	// - 3
		queue.add("jkl", 4);	// - - 2

		assertEquals("abc", queue.read());
		assertEquals("ghi", queue.read());
		queue.clear();
		assertEquals(0, queue.count());
	}

	@Test
	public void test_count_throughout()
	{
		queue = new PrioritizedStreamsQueue<String>(1, 5);
		queue.add("abc", 5);	// 0
		queue.add("def", 5);	// - 0
		queue.add("ghi", 4);	// - 3
		queue.add("jkl", 4);	// - - 2
		queue.add("mno", 3);	// - 1
		queue.add("pqr", 3);	// - - - 2
		queue.add("stu", 2);	// - - 1

		assertEquals(7, queue.count());
		assertEquals("abc", queue.read());
		assertEquals(6, queue.count());
		assertEquals("ghi", queue.read());
		assertEquals(5, queue.count());
		assertEquals("mno", queue.read());
		assertEquals(4, queue.count());
		assertEquals("def", queue.read());
		assertEquals(3, queue.count());
		assertEquals("jkl", queue.read());
		assertEquals(2, queue.count());
		assertEquals("stu", queue.read());
		assertEquals(1, queue.count());
		assertEquals("pqr", queue.read());
		assertEquals(0, queue.count());
		assertEquals(null, queue.read());
	}

}

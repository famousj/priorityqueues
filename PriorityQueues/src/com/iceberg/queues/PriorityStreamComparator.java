package com.iceberg.queues;

import java.util.Comparator;

/**
 * When more than one stream is ready to yield items in a cycle, this comparator determines
 * which item comes first. We use this and the item because it's the amount over the rollover
 * value that determines the current priority of a given item, not the priority of the
 * stream it came from (unless rollover priorities are equal).
 *
 * @param <T>
 */
public class PriorityStreamComparator<T>
		implements Comparator<PrioritizedStreamItem<T>>
{
	@Override
	public int compare(PrioritizedStreamItem<T> a, PrioritizedStreamItem<T> b)
	{
		if (a.priority == b.priority)
		{
			return a.streamPriority - b.streamPriority;
		}
		return a.priority - b.priority;
	}

}

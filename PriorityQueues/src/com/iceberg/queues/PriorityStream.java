package com.iceberg.queues;

import java.util.ArrayDeque;

/**
 * Each PriorityStream contains the items for a given priority (so one stream gets
 * created at each priority). The overall queue cycles a stream (one of these) to
 * up our currentValue. When that exceeds rollover value, we yield an item back
 * with a priority equal to the remainder from the rollover.
 * 
 * So, for example, if the rollover is 7 and our priority is 5, on the first cycle
 * we'll up our current value to 5 but not yield an item. On the second cycle we
 * up our current value to 10, which is over rollover, so the remainder is 3. The
 * value of 3 becomes our new current value and one item is yielded at priority 3.
 * On the third cycle, we up to 8, rollover, yield one item at remainder 1, which
 * is our new current value. Cycle 4 doesn't yield any items (current value 6
 * which is lower than the remainder).
 *
 * This is really an internal class to the overall streams queue, so shouldn't
 * be used directly for any other purpose. See @see PrioritizedStreamsQueue
 * for method descriptions, as these all basically come from there.
 * 
 * @param <T>
 */
public class PriorityStream<T>
{
	// this is the priority of this stream, gets added each cycle
	int myPriority;
	
	// this is the value at which we rollover, passing this value yields an entry
	private int rolloverValue;
	
	// this is our current item value, we add priority each cycle, and when we
	// pass our rollover value, we hand back an item at this priority
	private int currentValue;
	
	// internal queue to actually store items, in order (queue)
	private ArrayDeque<T> queue;
	
	PriorityStream(int priority, int rolloverValue)
	{
		this.myPriority = priority;
		this.rolloverValue = rolloverValue;
		this.currentValue = 0;
		
		this.queue = new ArrayDeque<T>();
	}
	
	public void add(T item)
	{
		queue.add(item);
	}
	
	public PrioritizedStreamItem<T> cycle()
	{
		if (queue.size() > 0)
		{
			currentValue += myPriority;
			if (currentValue >= rolloverValue)
			{
				return yieldItem();
			}
		}
		return null;
	}

	public int size()
	{
		return queue.size();
	}
	
	private PrioritizedStreamItem<T> yieldItem()
	{
		currentValue -= rolloverValue;
		PrioritizedStreamItem<T> yielded = new PrioritizedStreamItem<T>(queue.remove(), currentValue, myPriority);
		return yielded;
	}

	public int clear()
	{
		int count = queue.size();
		this.queue.clear();
		return count;
	}

	public void reset()
	{
		this.currentValue = 0;
	}
}

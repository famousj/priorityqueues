package com.iceberg.queues;

/**
 * When a stream is ready to yield an item, it yields the item in this structure.
 * We need to pass along the priority of the item from the stream.
 * NOTE --and this is really important-- the priority in this class is NOT the
 * priority of the stream the item came from. This is a priority based on
 * how far the value of the stream rolled over when yielding this item.
 * 
 * Because of this, a lower priority stream can yield an item at a higher
 * priority than a higher priority stream, assuming both were ready to yield
 * items at the same time. If two items have the same remainder priority,
 * then the one with higher stream priority goes first.
 * 
 * This is really an internal class to this package and shouldn't ever
 * be referenced outside of it.
 * 
 * @param <T>
 */
public class PrioritizedStreamItem<T>
{
	T item;
	int priority;
	int streamPriority;
	
	PrioritizedStreamItem(T item, int itemPriority, int streamPriority)
	{
		this.item = item;
		this.priority = itemPriority;
		this.streamPriority = streamPriority;
	}
}

package com.iceberg.queues;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * This is intended to be a thread-safe wrapper around the PrioritizedStreamsQueue class...
 * 
 * But then I decided that the PrioritizedStreamsQueue ought to just be implemented to be thread-safe (if needed).
 * 
 * This class could also serve as the "normal queue interface" wrapper around the PrioritizedStreamsQueue,
 * although in that case I'd rename it to indicate that purpose more clearly. This one also assumed a
 * priority range of 1-99, and then a default priority of 50 for items if the functions that include
 * a priority are not used.
 * 
 * Overall this class isn't really needed...
 *
 * @param <T>
 */
public class PriorityBlockingStreamsQueue<T> implements BlockingQueue<T>
{
	// lowest priority of any items inserted
	public static final int MINIMUM_PRIORITY = 1;
	
	// highest priority of any items inserted
	public static final int MAXIMUM_PRIORITY = 99;
	
	// if the default queue methods are used, this is the priority assigned to items
	public static final int DEFAULT_PRIORITY = 50;
	
	// this is the actual streams queue implementation
	private PrioritizedStreamsQueue<T> internalQueue;
	
	/*
	 * *********************************************************
	 * Constructor
	 * *********************************************************
	 */
	public PriorityBlockingStreamsQueue()
	{
		internalQueue = new PrioritizedStreamsQueue<T>(MINIMUM_PRIORITY, MAXIMUM_PRIORITY);
	}
	
	/*
	 * *********************************************************
	 * Custom queue add methods. These are the ones we'd prefer a client uses.
	 * *********************************************************
	 */
	
	/**
	 * Adds a single item to the queue with the given priority.
	 * 
	 * @param item
	 * @param priority
	 */
	public void add(T item, int priority)
	{
		internalQueue.add(item, priority);
	}
	
	/**
	 * Clears all items from the queue at the given priority level.
	 * Items of a different priority are unaffected.
	 * 
	 * @param priority
	 * @return the number of items removed from the queue
	 */
	public int clear(int priority)
	{
		return internalQueue.clear(priority);
	}
	
	/**
	 * Returns the next item based on the priority streams.
	 * 
	 * @return the next item based on priority streams. If not item is available returns null immediately.
	 */
	public T read()
	{
		return internalQueue.read();
	}
	
	/**
	 * Resets the priority yield of all streams. This clears any internal state
	 * that may be lingering. It's a good idea to call this any time you're
	 * ready to "start over" pulling new items while reusing a queue.
	 */
	public void reset()
	{
		internalQueue.reset();
	}
	
	/*
	 * *********************************************************
	 * BELOW HERE ARE METHODS INHERITED FROM BlockingQueue
	 * *********************************************************
	 */
	
	@Override
	public T element()
	{
		return internalQueue.read();
	}

	@Override
	public T peek()
	{
		return internalQueue.peek();
	}

	@Override
	public T poll()
	{
		return internalQueue.read();
	}

	@Override
	public T remove()
	{
		return internalQueue.read();
	}

	@Override
	public boolean addAll(Collection<? extends T> items)
	{
		for (T item : items)
		{
			internalQueue.add(item, DEFAULT_PRIORITY);
		}
		return items.size() > 0;
	}

	@Override
	public void clear()
	{
		internalQueue.clear();
	}

	@Override
	public boolean containsAll(Collection<?> arg0)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty()
	{
		return internalQueue.count() == 0;
	}

	@Override
	public Iterator<T> iterator()
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> arg0)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> arg0)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int size()
	{
		return internalQueue.count();
	}

	@Override
	public Object[] toArray()
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@SuppressWarnings("hiding")
	@Override
	public <T> T[] toArray(T[] arg0)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public boolean add(T item)
	{
		internalQueue.add(item, DEFAULT_PRIORITY);
		return true;
	}

	@Override
	public boolean contains(Object arg0)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public int drainTo(Collection<? super T> sink)
	{
		int count = 0;
		T item = null;
		while ((item = internalQueue.read())!= null)
		{
			sink.add(item);
			count++;
		}
		return count;
	}

	@Override
	public int drainTo(Collection<? super T> sink, int max)
	{
		int count = 0;
		T item = null;
		while ((count < max) && ((item = internalQueue.read())!= null))
		{
			sink.add(item);
			count++;
		}
		return count;
	}

	@Override
	public boolean offer(T item)
	{
		internalQueue.add(item, DEFAULT_PRIORITY);
		return true;
	}

	@Override
	public boolean offer(T item, long arg1, TimeUnit arg2)
			throws InterruptedException
	{
		internalQueue.add(item, DEFAULT_PRIORITY);
		return true;
	}

	@Override
	public T poll(long timeout, TimeUnit unit) throws InterruptedException
	{
		return internalQueue.poll(timeout, unit);
	}

	@Override
	public void put(T item) throws InterruptedException
	{
		internalQueue.add(item, DEFAULT_PRIORITY);
	}

	@Override
	public int remainingCapacity()
	{
		return Integer.MAX_VALUE;
	}

	@Override
	public boolean remove(Object arg0)
	{
		throw new java.lang.UnsupportedOperationException();
	}

	@Override
	public T take() throws InterruptedException
	{
		return internalQueue.poll(-1, TimeUnit.DAYS);
	}

}

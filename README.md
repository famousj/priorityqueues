This code is Java classes to setup prioritized queues. That is, you add items to
queues with a given priority, then you can pull things from the queue and they
come out based on relative priority.

Items come out based on the priority of their queue, but one priority can't
starve out other priorities. So, for example, if you have items at priority 8
and 5, for every 8 items pulled at priority 8, you'll get 5 items pulled at
priority 5. Works that way for any priority. If you have priority 9 and 2,
you'll get 9 items at priority 9 for every 2 at priority 2.

This this allows you to feed in data of different priorities, pull it off
guaranteeing more higher priority items first, but also guaranteeing that
you don't starve out lower priority items. The ratio is based on the
relative priority of the items.
